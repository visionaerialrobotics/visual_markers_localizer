cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneVisualMarkersLocalizer)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries





set(LOCALIZATION_LIB_SOURCE_DIR
	src/sources)
	
set(LOCALIZATION_LIB_INCLUDE_DIR
	src/include
	)

set(LOCALIZATION_LIB_SOURCE_FILES

	#General
	src/sources/Kalman_Loc2.cpp
	
	src/sources/tools.cpp
	#src/sources/observation3d.cpp
	src/sources/landmark3d.cpp
	src/sources/htrans.cpp
	src/sources/location.cpp
	
	#src/sources/droneLocalizer.cpp
	
	#src/sources/droneLocalizerNode.cpp
	
	)
	
set(LOCALIZATION_LIB_HEADER_FILES

	#General
	src/include/Kalman_Loc2.h
	
	src/include/tools.h
	src/include/observation3d.h
	src/include/landmark3d.h
	src/include/htrans.h
	src/include/location.h
	
	#src/include/droneLocalizer.h
	
	)
	
	
	
find_package(catkin REQUIRED
		COMPONENTS pugixml lib_newmat11)

catkin_package(
        INCLUDE_DIRS ${LOCALIZATION_LIB_INCLUDE_DIR}
        LIBRARIES ${PROJECT_NAME}
        CATKIN_DEPENDS pugixml lib_newmat11
  )


include_directories(${LOCALIZATION_LIB_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})



add_library(${PROJECT_NAME} ${LOCALIZATION_LIB_SOURCE_FILES} ${LOCALIZATION_LIB_HEADER_FILES})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})
